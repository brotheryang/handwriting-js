const arr = [
  1,
  2,
  3,
  [4, 5, [45, 56, [54, 87, [0, [88, 45]]]], [9], [12]],
  7,
  [32, 45, 54, 56],
  [34],
]

function _flat(arr) {
  return arr.reduce((perv, next) => {
    if (Array.isArray(next)) {
      perv.push(..._flat(next))
    } else {
      perv.push(next)
    }
    return perv
  }, [])
}

export default _flat
