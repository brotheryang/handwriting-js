const obj = { name: 'Bob', age: 15 }

obj[Symbol.iterator] = function* () {
  const values = Object.values(this)
  for (let i = 0; i < values?.length; i++) {
    yield values[i]
  }
}

for (const item of obj) {
  console.log(item, 'item')
}

export default obj
