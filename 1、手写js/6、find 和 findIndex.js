export function _find(func) {
  for (let i = 0; i < this.length; i++) {
    if (func(this[i], i, this)) return this[i]
  }
}

export function _findIndex(func) {
  for (let i = 0; i < this.length; i++) {
    if (func(this[i], i, this)) return i
  }

  return -1
}
