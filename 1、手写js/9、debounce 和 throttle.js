export function _debounce(func, delay) {
  let timer
  return function () {
    if (timer) clearInterval(timer)
    timer = setTimeout(() => {
      // this指向
      func.apply(this, arguments)
    }, delay)
  }
}

export function _throttle(func, delay) {
  let timer
  return function () {
    if (timer) return
    timer = setTimeout(() => {
      // this指向
      func.apply(this, arguments)
      timer = null
    }, delay)
  }
}
