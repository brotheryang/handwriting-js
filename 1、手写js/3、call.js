export default function (context, ..._args) {
  context = context === null || context === undefined ? window : Object(context)
  const func = Symbol()
  // this指向问题
  context[func] = this
  return context[func](..._args)
}
