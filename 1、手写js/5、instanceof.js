export default function (A, B) {
  let a = Object.getPrototypeOf(A)
  const b = B.prototype

  while (a !== b) {
    if (a === null) return false
    a = Object.getPrototypeOf(a)
  }

  return true
}
