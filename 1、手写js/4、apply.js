export default function (context, _args) {
  if (!Array.isArray(_args)) throw 'CreateListFromArrayLike called on non-object'
  context = context === null || context === undefined ? window : Object(context)
  const func = Symbol()
  // this指向问题
  context[func] = this
  return context[func](..._args)
}
