function create(proto, propertiesObject = undefined) {
  // proto 新创建对象的原型对象, propertiesObject 要定义其可枚举属性或修改的属性描述符的对象
  if (typeof proto !== 'object' && proto !== null && typeof proto !== 'function')
    // 只能是 null 或者 object
    throw Error('Uncaught TypeError: Object prototype may only be an Object or null')

  function F() {} // 创建一个空的构造函数 F
  F.prototype = proto // F 原型指向 proto
  const obj = new F() // 创建 F 的实例

  if (propertiesObject !== undefined)
    // propertiesObject有值则调用 Object.defineProperties
    Object.defineProperties(obj, propertiesObject)

  return obj // 返回 这个 obj
}

export default create
