// const a = {
//   i: 1,
//   valueOf() {
//     return this.i++
//   },
// }

// 隐士类型转换，数组会调join
// const a = [1, 2, 3]
// a.join = a.shift

let i = 1
Number.prototype.valueOf = function () {
  return i++
}
const a = new Number()

if (a == 1 && a == 2 && a == 3) {
  console.log('hello world')
}

// export default {}
